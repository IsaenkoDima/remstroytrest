var app = {
  init: function () {
    app.windowResize();
    app.fullScreenHeight();
    app.modals();
    app.menu();
    app.custom();
    app.sliders();
    app.tabs();
    app.validate();
    app.accordeon();
  },

  windowResize: function () {
    $(window).on('resize', function () {
      app.fullScreenHeight();
    });
  },

  windowLoad: function () {
    $(window).on('load', function () {
      app.fullScreenHeight();
    });
  },

  fullScreenHeight: function () {
    var headHeight = 0, footHeight = 0;
    if ($('header').length) { headHeight = $('header').outerHeight(); }
    if ($('footer').length) { footHeight = $('footer').outerHeight(); }

    const height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    $('.jsHeightWithoutHeader').css('min-height', height - headHeight);
    $('.jsHeightHF').css('min-height', height - headHeight - footHeight);
  },

  
  menu: function () {
    var $btnMenu = $('.jsMenu');
    $btnMenu.on('click', function () {
      $(this).toggleClass('menu-is-active');
      $('body').toggleClass('menuopen');
      $('#main_mobile_menu').fadeToggle(200);
    });

    $('.jsCloseMenu').each(function(){
      var $this = $(this);
      $this.on('click', function(){
        $this.parents('.b_mobile_menu').fadeOut(200);
        $('body').removeClass('menuopen');
      });
    });

    $('.jsOpenMenuCatecory').on('click', function(){
      $('#categories_mobile_menu').fadeToggle(200);
      $('body').toggleClass('menuopen');
    });
  },

  custom: function () {
    $('[data-fancybox]').fancybox({
      backFocus: false
    });
  },


  sliders: function sliders() {
    
    $('.jsBannersSlider').each(function(){
      var $this = $(this);
      var $thisSpeed = $(this).attr('data-speed');
      $this.slick({
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        touchThreshold: 150,
        autoplay: true,
        autoplaySpeed: $thisSpeed,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
            }
          }
        ]
      });
    });

    
    
    $('.jsOfferWrapper').each( function() {
      var $this = $(this),
          $thisSlider = $this.find('.jsOfferSlider'),
          $btnPrev = $this.find('.btn_prev'),
          $btnNext = $this.find('.btn_next');

      $thisSlider.slick({
        // arrows: false,
        prevArrow: $btnPrev,
        nextArrow: $btnNext,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        touchThreshold: 150,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 3,
            }
          },
          {
            breakpoint: 500,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 400,
            settings: {
              slidesToShow: 1,
            }
          }
        ]
      });
    });


    $('.jsCertificatesSlider').slick({
      arrows: false,
      dots: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      touchThreshold: 150,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 400,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });



    $('.jsSliderSection').each(function(){
      var $this = $(this),
          $slider = $this.find('.jsOurWorksSlider'),
          $status = $this.find('.jsSliderCounter'),
          $btnPrev = $this.find('.btn_prev'),
          $btnNext = $this.find('.btn_next');

      $status.each(function(){
        var number = $slider.find('.slide_item').length;
        $(this).text(number);
      });

      $slider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        var i = (currentSlide ? currentSlide : 0) + 1;
        $this.find('.current').text(i);
      });
      
      $slider.slick({
        prevArrow: $btnPrev,
        nextArrow: $btnNext,
        // arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        touchThreshold: 150
      });
    });



    $(window).on('resize load', function () {


      $('.jsSteps').each(function(){
        var $thisCategoriesList = $(this);
        
        if ($(window).width() <= 768) {
          if (!$thisCategoriesList.hasClass('slick-initialized')) {
            $thisCategoriesList.slick({
              slidesToShow: 1,
              infinite: false,
              slidesToScroll: 1,
              touchThreshold: 150,
              arrows: false,
              dots: true
            });
          }
        } else {
          if ($thisCategoriesList.hasClass('slick-initialized')) {
            $thisCategoriesList.slick('unslick');
          }
        }
      });

    });
  },


  modals: function () {
    var startWindowScroll = 0;

    $('.jsOpenModals').magnificPopup({
      removalDelay: 300,
      mainClass: 'my-mfp-slide-bottom',
      fixedContentPos: true
    });

    $('.jsCloseModal').each(function(){
      var $this = $(this);
      $this.on('click', function(){
        $this.parents('.b_modal').magnificPopup('close');
      });
    });
  },


  tabs: function () {
    var tabs = $('.jsTabs');
    tabs.each(function () {
      var tabs = $(this),
        tab = tabs.find('.jsTabsTab'),
        content = tabs.find('.jsTabsItem');
      tab.each(function (index, element) {
        $(this).attr('data-tab', index);
      });

      function showContent(i) {
        tab.removeClass('-active');
        content.removeClass('-active').removeClass('-fade');
        tab.eq(i).addClass('-active');
        content.eq(i).addClass('-active');
        setTimeout(function () {
          content.eq(i).addClass('-fade');
        }, 1);
      }
      tab.on('click', function (e) {
        e.preventDefault();
        showContent(parseInt($(this).attr('data-tab')));
      });
    });
  },

  validate: function () {

    $('input[type="tel"]').mask('+7 (999) - 999 - 99 - 99');
    
  },


  accordeon: function () {
    $('.jsAccord').each(function () {
      var accord = $(this),
        accord_btn = accord.find('.jsAccordBtn'),
        accord_content = accord.find('.jsAccordContent'),
        accord_item = accord.find('.jsAccordItem');

      accord_btn.on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
          $this_item = $this.closest('.jsAccordItem'),
          $this_content = $this.closest('.jsAccordItem').find('.jsAccordContent');
        if ($this.hasClass('-active')) {
          $this.removeClass('-active');
          $this_content.slideUp();
          $this_item.removeClass('item_active');
        } else {
          accord_item.removeClass('item_active');
          accord_btn.removeClass('-active');
          accord_content.slideUp();
          $this.addClass('-active');
          $this_content.slideDown();
          $this_item.addClass('item_active');
        }
      });
    });
  },


  map: function map() {
    // if ($('#map').length) {
    //   var initMap = function initMap() {

    //     var myLatLng = { lat: $datalat, lng: $datalng };

    //     var mapOptions = {
    //       scrollwheel: false,
    //       zoom: 13,
    //       center: myLatLng
    //     };

    //     var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    //     var marker = new google.maps.Marker({
    //       position: myLatLng,
    //       map: map
    //     });
    //   };

    //   if ($('#map').attr('data-lat') !== '' && $('#map').attr('data-lng') !== '') {
    //     var $datalat = parseFloat($('#map').attr('data-lat')),
    //         $datalng = parseFloat($('#map').attr('data-lng'));
    //   } else {
    //     var $datalat = 1,
    //         $datalng = 1;
    //   }

    //   google.maps.event.addDomListener(window, 'load', initMap);
    // }
  }

};

$(document).ready(app.init());

app.windowLoad();
